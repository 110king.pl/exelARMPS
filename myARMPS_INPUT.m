clear
clc
%Defined output file
delete('ARMPS_INPUT.dat')
diary('ARMPS_INPUT.dat')
diary on
%Importing input data
inputfile = importdata('ARMPS_INPUT.xls');
title           = cell2mat(inputfile.textdata(3,1));
comment         = cell2mat(inputfile.textdata(4,1));
units           = inputfile.data(5,1);
entryHight      = inputfile.data(8,1);
depthOfCover    = inputfile.data(9,1);
crosscutAngle   = inputfile.data(10,1);
entryWidth      = inputfile.data(11,1);
crosscutSpacing = inputfile.data(12,1);
numberOfEntry   = inputfile.data(13,1);
entryDistance   = inputfile.data(14,1);
averageExtractRatio     = entryDistance -1; %must fix
coalStrength            = inputfile.data(16,1);
overburdenUnitWeight    = inputfile.data(17,1);
activeMiningZoneBreath  = inputfile.data(18,1);
pressureArchFactor      = inputfile.data(19,1);
loadingCondition        = inputfile.data(21,1);
LC1_extendOfGob         = inputfile.data(23,1);
LC1_abundmentAngle      = inputfile.data(24,1);
LC2_extendOftGob        = inputfile.data(26,1);
LC2_abundmentAngle      = inputfile.data(27,1);
LC2_barrierPillar       = inputfile.data(28,1);
LC2_slabCut             = inputfile.data(29,1);
LC3_extendOftGob        = inputfile.data(31,1);
LC3_abundmentAngle      = inputfile.data(32,1);
LC3_barrierPillar       = inputfile.data(33,1);
LC3_slabCut             = inputfile.data(34,1);
rowA_active     = inputfile.data(36,1);
rowB_active     = inputfile.data(37,1);
rowC_active     = inputfile.data(38,1);
rowD_active     = inputfile.data(39,1);
rowC_width      = inputfile.data(40,1);
rowD_width      = inputfile.data(41,1);

%Generate ARMPS output file
disp(sprintf('ARMPS_602'));
disp(sprintf(' %i',units));
disp(sprintf('%s',title));
disp(sprintf('##StartDesc##'));
disp(sprintf('%s',comment));
disp(sprintf('##EndDesc##'));
disp(sprintf('%i',entryHight));
disp(sprintf('%i',depthOfCover));
disp(sprintf('%i',crosscutAngle));
disp(sprintf('%i',entryWidth));
disp(sprintf('%i',crosscutSpacing));
disp(sprintf(' %i',numberOfEntry));
for i=1:numberOfEntry-1
    disp(sprintf('%i',entryDistance));
end
disp(sprintf('%i',coalStrength));
disp(sprintf('%i',overburdenUnitWeight));
disp(sprintf('%i',activeMiningZoneBreath));
disp(sprintf(' 0')); %1- Automatic AMZ
disp(sprintf(' 0')); %1- Automatic pressure Arch Factor
%loading condition
% 0 - Development Load (no nearby gob)
% 1 - One active retreat section
% 2 - Once active reatreat + one side gob
% 3 - One active reatreat + two side gob
disp(sprintf(' %i',loadingCondition));
disp(sprintf(' 0'));
disp(sprintf(' %2.2f',pressureArchFactor));
disp(sprintf('%i',LC1_extendOfGob));
disp(sprintf('%i',LC1_abundmentAngle));
disp(sprintf('%i',LC2_extendOftGob));
disp(sprintf('%i',LC2_abundmentAngle));
disp(sprintf('%i',LC2_barrierPillar));
disp(sprintf('%i',LC2_slabCut));
disp(sprintf('%i',LC3_extendOftGob));
disp(sprintf('%i',LC3_abundmentAngle));
disp(sprintf('%i',LC3_barrierPillar));
disp(sprintf('%i',LC3_slabCut));
disp(sprintf(' %i',rowA_active));  %A
disp(sprintf(' %i',rowB_active));  %B
disp(sprintf(' %i',rowC_active));  %C
disp(sprintf(' %i',rowD_active));  %D
disp(sprintf('%i',rowC_width));
disp(sprintf('%i',rowD_width));
